import edu.princeton.cs.algs4.WeightedQuickUnionUF;
import edu.princeton.cs.algs4.StdOut;
// import java.lang;

public class Percolation { //

    private final int top; //
    private boolean[][] opened; //
    private final int bottom; //
    private final int size; //
    private final WeightedQuickUnionUF qf; // WeightedQuickUnionUF

    //
    public Percolation(int n) //
    {
        top = 0;
        size = n;
        bottom = n * n + 1;

        if (n <= 0) {
            throw new IllegalArgumentException();
        }

        qf = new WeightedQuickUnionUF(n * n + 2);
        opened = new boolean[n][n];
    }

    //
    public void open(int row, int col)// open site (row, col) if it is not open already
    {
        opened[row - 1][col - 1] = true;

        if (row == 1)
            qf.union(getQFIndex(row, col), top);

        if (row == size)
            qf.union(getQFIndex(row, col), bottom);

        if (row > 1 && isOpen(row - 1, col))
            qf.union(getQFIndex(row, col), getQFIndex(row - 1, col));

        if (row < size && isOpen(row + 1, col))
            qf.union(getQFIndex(row, col), getQFIndex(row + 1, col));

        if (col > 1 && isOpen(row, col - 1))
            qf.union(getQFIndex(row, col), getQFIndex(row, col - 1));

        if (col < size && isOpen(row, col + 1))
            qf.union(getQFIndex(row, col), getQFIndex(row, col + 1));

    }

    //
    public boolean isOpen(int row, int col)  // is site (row, col) open?
    {

        if (row > 0 && row <= size && col > 0 && col <= size)  {
            return opened[row - 1][col - 1];
        }
        else {
            throw new IllegalArgumentException();
        }
    }

    //
    public boolean isFull(int row, int col)  // is site (row, col) full?
    {

        if (row > 0 && row <= size && col > 0 && col <= size)  {
            return qf.connected(top, getQFIndex(row, col));
        }
        else {
            throw new IllegalArgumentException();
        }
    }

    //
    public int numberOfOpenSites() // number of open sites
    {
        int count = 0;

        for (int i = 1; i <= size; i++)
            for (int j = 1; j <= size; j++) {
                if (isOpen(i, j))
                    count++;
            }

        return count;
    }

    //
    public boolean percolates() // does the system percolate?
    {
        return qf.connected(top, bottom);
    }

    //
    private int getQFIndex(int row, int col) {
        return size * (row - 1) + col - 1 + 1; // top is 0 => + 1
    }


    //
    public static void main(String[] args)
    {
        int n = Integer.parseInt(args[0]);

        Percolation per = new Percolation(n);

        if (per.isOpen(n-1, n-1)) {
            StdOut.println("it is opened.");
        }

    }

}
