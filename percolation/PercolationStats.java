import edu.princeton.cs.algs4.StdRandom;
import edu.princeton.cs.algs4.StdStats;
import edu.princeton.cs.algs4.StdOut;

public class PercolationStats {

    private final int count; //
    private final double[] fractions; //
    // private Percolation m_pr; //
    private final double constant;

    //
    public PercolationStats(int n, int trials)
    {
        constant = 1.96d;

        if (n <= 0 || trials <= 0) {
            throw new IllegalArgumentException("N <= 0 || T <= 0");
        }

        count = trials;
        fractions = new double[count];

        for (int i = 0; i < count; i++) {

            Percolation pr = new Percolation(n);
            int openedSites = 0;

            while (!pr.percolates()) {
                int row = StdRandom.uniform(1, n + 1);
                int col = StdRandom.uniform(1, n + 1);

                if (!pr.isOpen(row, col)) {
                    pr.open(row, col);
                    openedSites++;
                }
            }

            fractions[i] = (double) openedSites / (n * n);
        }
    }

    //
    public double mean()
    {
        return StdStats.mean(fractions);
    }

    //
    public double stddev()
    {
        return StdStats.stddev(fractions);
    }

    //
    public double confidenceLo()
    {
        return mean() - constant * stddev() / Math.sqrt(count);
    }

    //
    public double confidenceHi()
    {
        return mean() + constant * stddev() / Math.sqrt(count);
    }


    public static void main(String[] args)
    {
        int n = Integer.parseInt(args[0]);
        int t = Integer.parseInt(args[1]);

        PercolationStats ps = new PercolationStats(n, t);

        String confid = ps.confidenceLo() + ", " + ps.confidenceHi();
        StdOut.println("mean                    = " + ps.mean());
        StdOut.println("stddev                  = " + ps.stddev());
        StdOut.println("95% confidence interval = [" + confid + "]");
    }
}
